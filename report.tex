\documentclass{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\newcommand{\fol}{\mbox{FOL}}	
\newcommand{\theory}{\mbox{Th}}	
\newcommand{\tower}{\mbox{Tower}}	
\newcommand{\encoding}{encoding_h(x)}	
\newcommand{\maxx}{max_h(x)}	
\newcommand{\succc}{succ_h(x, y)}	

\newcommand{\sh}[1]{\todo[color=yellow!40!white]{\scriptsize SH:  #1}{}}

%opening
\title{Using Hintikka sentences to \\ construct reduction sequences for Feferman-Vaught theorem is efficient}
\author{Shengyu Huang}

\begin{document}

\maketitle

Let $\tau$ be a fixed and finite vocabulary and $\fol(\tau)$ denote the set of $\tau$-sentences in first-order logic. In addition, $\fol^q(\tau)$ denotes the first-order sentences of quantifier rank at most $q \in \mathbb{N}$.

For a class of $\tau$-structures $K$, $\theory(K)$ is the set of sentences in $\fol(\tau)$ that are true in all $\mathcal{U} \in K$. We write $\theory(\mathcal{U})$ if $K = \{\mathcal{U}\}$. Additionally, sentences in $\theory(\mathcal{U})$ that are of quantifier rank at most $q$ are written as $\theory^q(\mathcal{U})$. Sentences in $\theory(\mathcal{U})$ that are of length at most $l$ are written as $\theory_l(\mathcal{U})$. 

We define the size (or, length) $|\varphi|$ of a first-order formula $\varphi$ as the number of vertices of $\varphi$'s syntax tree. In this report, we want to investigate the formula length of the reduction sequences in the computational proof of Feferman-Vaught theorem \cite{feferman1967first}.

\section{Using Hintikka sentences to construct reduction sequences for Feferman-Vaught theorem}

\begin{theorem}\label{feferman-vaught}[Feferman-Vaught theorem for first-order logic]
	For all structures $\mathcal{A}_1, \mathcal{A}_2, \mathcal{B}_1, \mathcal{B}_2$, if $\theory(\mathcal{A}_1) = \theory(\mathcal{B}_1)$ and $\theory(\mathcal{A}_2) = \theory(\mathcal{B}_2)$, then $\theory(\mathcal{A}_1 \sqcup \mathcal{A}_2) = \theory(\mathcal{B}_1 \sqcup \mathcal{B}_2)$. In other words, for two structures $\mathcal{A}$ and $\mathcal{B}$, $\theory(\mathcal{A} \sqcup \mathcal{B})$ are uniquely determined by $\theory(\mathcal{A})$ and $\theory(\mathcal{B})$.
	
	By stratifying the quantifier rank, we can say for two structures $\mathcal{A}$ and $\mathcal{B}$, $\theory^q(\mathcal{A} \sqcup \mathcal{B})$ are uniquely determined by $\theory^q(\mathcal{A})$ and $\theory^q(\mathcal{B})$ for all $q \in \mathbb{N}$.
\end{theorem}

Theorem~\ref{feferman-vaught} can be proved with Ehrenfeucht–Fraïssé game by just putting the winning strategies for $\theory^q(\mathcal{A})$ and $\theory^q(\mathcal{B})$ together. A different, computational proof by constructing reduction sequences is presented in \cite{feferman1967first} and gives the following results.

\begin{theorem}
	For every $q \in \mathbb{N}$ and every formula $\theta \in \fol^q(\tau)$ one can compute effectively a reduction sequence and a boolean function $B_{\theta}: \{0, 1\}^{2m} \rightarrow \{0, 1\}$. Specifically, a reduction sequence is a sequence of formulas of the form
	
	$$\langle \psi_1^\mathcal{A}, ..., \psi_m^\mathcal{A}, \psi_1^\mathcal{B}, ..., \psi_m^\mathcal{B} \rangle,$$
	
	where $\psi_i^\mathcal{U} \in \fol^q(\tau)$ for $i \in \{1, ..., m\}$ and $\mathcal{U} \in \{\mathcal{A}, \mathcal{B}\}$.
	
	For the boolean function $B_{\theta}: \{0, 1\}^{2m} \rightarrow \{0, 1\}$, we have
	$$\mathcal{A} \sqcup \mathcal{B} \models \theta \text{ iff } B_\theta(b_1^\mathcal{A}, ..., b_m^\mathcal{A}, b_1^\mathcal{B}, ..., b_m^\mathcal{B}) = 1,$$
	
	where $b_j^\mathcal{A} = 1$ iff $\mathcal{A} \models \psi_j^\mathcal{A}$ and $b_j^\mathcal{B} = 1$ iff $\mathcal{B} \models \psi_j^\mathcal{B}$.
\end{theorem}

For a fixed and finite vocabulary, there are only finitely many Hintikka sentences of quantifier rank $q$. We enumerate them as $\{h_1^q, ..., h_{\alpha}^q\}$. Theorem~\ref{feferman-vaught} implies there is a function $f: [\alpha]^2 \rightarrow [\alpha]$ such that $\mathcal{A} \models h_i^q$ and $\mathcal{B} \models h_j^q$ iff $\mathcal{A} \sqcup \mathcal{B} \models h^q_{f(i, j)}$. Using Hintikka sentences to construct the reduction sequences for the formula $\theta$ of length $n$ will yield a formula of length $\tower(n, n)$, where $\tower(1, n) = O(2^n)$ and $\tower(i, n) = O(2^{\tower(i-1, n)})$ for all $i > 1$. 

Surprisingly, this upper bound is essentially tight in the following sense.

\begin{theorem}\label{main-theorem}\cite[Theorem 6.1]{dawar2007model}
	Let $\mathfrak{T}$ denote the class of all finite trees. There is no elementary function $f$ such that the following holds for all trees $\mathcal{A}, \mathcal{B}, \mathcal{C} \in \mathfrak{T}$: if $\theory_{f(l)}(\mathcal{A}) = \theory_{f(l)}(\mathcal{B})$, then $\theory_l(\mathcal{A} \sqcup \mathcal{C}) = \theory_l(\mathcal{B} \sqcup \mathcal{C})$.
\end{theorem}

Before proving Theorem~\ref{main-theorem}, we first establish some definitions appeared in it and a key technique that will be used in the proof in Section~\ref{proof}. For succinctness, we abbreviate $\tower(n, 1)$ as $\tower(n)$ from now on.


\section{Preliminaries}

\begin{definition}[$k$-fold exponential function]
	A function $f: \mathbb{N} \rightarrow \mathbb{R}$ is called \emph{($1$-fold) exponential} if there is some polynomial $p$ such that $f(n)$ is eventually bounded by $2^{p(n)}$. For $k \geq 2$, $f$ is called \emph{$k$-fold exponential} if there is some $(k-1)$-fold exponential function $g$ such that $f(n)$ is eventually bounded by $2^{g(n)}$.
\end{definition}

\begin{definition}[Elementary function]
	A function $f: \mathbb{N} \rightarrow \mathbb{R}$ is called \emph{elementary} if it can be formed from the successor function, addition, subtraction, and multiplications using compositions, projections, bounded additions, and bounded multiplications. A function $f$ is bounded by an elementary function iff there exists a $k \geq 1$ such that $f$ is bounded by a $k$-fold exponential function.
\end{definition}


\subsection{Encoding numbers by trees}\label{tree-encoding}

For every number $n \in \mathbb{N}$, we define a tree $\mathcal{T}(n)$ as follows.
\begin{itemize}
	\item $\mathcal{T}(0)$ is the node-node tree.
	\item For $n \geq 1$ the tree $\mathcal{T}$ is obtained by creating a new root and attaching it to every tree $\mathcal{T}(i)$ such that the $i$-th bit in the binary representation of $n$ is $1$.
\end{itemize}

Recall that the height of a tree $\mathcal{T}$ is the length of the longest path in $\mathcal{T}$. Observe that we can encode numbers as big as $\tower(h)$ with a tree of height at most $h$. In fact, the other way is also true, i.e., if the height of a tree $\mathcal{T}$ is at most $h$, the number it represents must be less than or equal to $\tower(h)$.

\begin{lemma}\label{tree-height-power}\cite[Lemma 10.20]{flum2006}
	For all $h, n \geq 0$, $height(\mathcal{T}(n)) \leq h \iff n \leq \tower(h)$.
\end{lemma}

Encoding numbers by trees gives us some small first-order formulas that will come in handy for the proof in Section~\ref{proof}. Specifically, we will use a compound formula that consists of three first-order formulas $\encoding$, $\succc$, and $\maxx$.

Intuitively, $\encoding$ holds, if a tree with $x$ as its root is a valid encoding of some number $n < \tower(h)$. By Lemma~\ref{tree-height-power}, it also implies that such a tree has a height strictly less than $h$. The meanings for $\succc$ and $\maxx$ are self-explanatory, and we will give their formal definitions in Lemma~\ref{max} and Lemma~\ref{succ}, respectively.

In the following text, we use $E$ to denote a binary relation symbol and view trees as being directed from the root to leafs. 

For a directed graph $\mathcal{A} = (A, E^\mathcal{A})$ and an $a \in A$, we use $A_a$ to be the set of all vertices $b$ such that there is a path from $a$ to $b$. $\mathcal{A}_a$ is the induced substructure of $\mathcal{A}$ with universe $A_a$. Note that there is always a path from a node to itself.

\begin{lemma}\label{encoding}
	For all structures $\mathcal{A} = (A, E^{\mathcal{A}})$ and $x \in A$ there is a first-order formula $\encoding$ of length $O(h^2)$ for every $h \geq 0$ such that $\mathcal{A} \models \encoding$ iff $\mathcal{A}_x$ is isomorphic to $\mathcal{T}(i)$ for some $i \in \{0, ..., \tower(h) -1 \}$.
\end{lemma}

\begin{proof}
	For an arbitrary structure $\mathcal{A} = (A, E^\mathcal{A})$ and $x \in A$, we define $\encoding$ inductively as follows. 
	
	When $h = 0$, $\tower(h) = 1$. We want $\mathcal{A} \models  encoding_0(x)$ iff $\mathcal{A}_x$ is isomorphic to $\mathcal{T}(0)$. Since $\mathcal{T}(0)$ is the one-node tree, we can simply choose $encoding_0(x) := \lnot \exists y E(x, y)$.
	
	For $h \geq 1$, we define $\encoding := \forall y \big(E(x, y) \rightarrow encoding_{h-1}(y)\big) \land \forall y \forall y' \big( (E(x, y) \land E(x, y') \land \lnot y= y' )\rightarrow \lnot eq_{h-1}(y, y') \big)$.
	
	We introduce $eq_{h-1}(y, y')$ to express the notion of equality. Formally, for every structures $\mathcal{A} = (A, E^\mathcal{A})$ and $x, y \in A$ if there are $m, n < \tower(h)$ such that $\mathcal{A}_x$ and $\mathcal{A}_y$ are isomorphic to $\mathcal{T}_m$ and $\mathcal{T}_n$, respectively, then $\mathcal{A} \models eq_h(x, y) \iff m =n$. In addition, the length of $eq_h(x, y)$ is $O(h)$ for every $h \geq 1$~\cite[Lemma 10.21]{flum2006}.
	
	Based on the definition of $\encoding$, we know there exists a constant $c > 0$ such that $|encoding_h| \leq |encoding_{h-1}| + |eq_{h-1}| + c$. Solving this recurrence equation gives us $$|encoding_h| \leq |encoding_0| + \sum_{i=0}^{h-1}|eq_i| + c \cdot h = O(h^2).$$
	
\end{proof}

In a similar way, we can show the existence of $\succc$ and $\maxx$ and derive the upper bound for their formula lengths. We refer readers to the original paper for more detailed proofs.

\begin{lemma}\label{succ}\cite[Lemma 3.4]{dawar2007model}
	For all structures $\mathcal{A} = (A, E^{\mathcal{A}})$ and $x, y \in A$  there is a first-order formula $\succc$ of size $O(h^3)$ for every $h \geq 0$ such that $\mathcal{A} \models \succc$ iff $m + 1 = n$, 
   where $m , n < \tower(h)$ and $\mathcal{A}_x$ and $\mathcal{A}_y$ are isomorphic to $\mathcal{T}(m)$ and $\mathcal{T}(n)$, respectively.
\end{lemma}

\begin{lemma}\label{max}\cite[Lemma 3.4]{dawar2007model}
	For all structures $\mathcal{A} = (A, E^{\mathcal{A}})$ and $x \in A$  there is a first-order formula $\maxx$ of size $O(h^4)$ for every $h \geq 0$ such that $\mathcal{A} \models \maxx$ iff $\mathcal{A}_x	\text{ is isomorphic to } \mathcal{T}(\tower(h) - 1)$.
\end{lemma}

\section{Lower bound of the reduction sequences}\label{proof}

We are ready to prove Theorem~\ref{main-theorem}. Assume $f$ is elementary for contradiction. Continuing to use $\mathfrak{T}$ to denote the class of all finite trees, we will show the existence of two structures $\mathcal{A}, \mathcal{B} \in \mathfrak{T}$ such that $\theory_{f(c \cdot h^4)}(\mathcal{A}) = \theory_{f(c \cdot h^4)}(\mathcal{B})$.  Then we give a structure $\mathcal{C} \in \mathfrak{T}$ and a first-order formula $\varphi_h$ of length at most $O(c \cdot h^4)$ for some $c \geq 1$ and for every $h \geq 0$ such that $\mathcal{A} \sqcup \mathcal{C} \models \varphi_h$ and $\mathcal{B} \sqcup \mathcal{C} \not\models \varphi_h$, i.e., $\theory_{c \cdot h^4}(\mathcal{A} \sqcup \mathcal{C}) \neq \theory_{c \cdot h^4}(\mathcal{B} \sqcup \mathcal{C})$.

Specifically, 
$$\varphi_h := \forall x\big(\encoding \rightarrow \big(\maxx \lor \exists y \; \succc \big)\big).$$

The upper bound of the length of $\varphi_h$ directly follows from Lemma~\ref{encoding}, Lemma~\ref{succ}, and Lemma~\ref{max}.

\begin{lemma}\label{upper-bound-formula}
	There is a constant $c \geq 1$ such that $|\varphi_h| \leq c \cdot h^4$ for all $h \geq 0$.
\end{lemma}

	 We fix an $h$ such that $|\theory_{f(c \cdot h^4)}(\mathfrak{T})| \leq \tower(h) -1$. Such an $h$ is guaranteed to exist because there are only exponentially many first-order sentences of a given length bounded by a $k$-fold exponential function for some $k \geq 1$, while $\tower(h)$ is not bounded by any $k$-fold exponential function.
	
	Next, we construct a set of structures $\mathcal{F}$ as follows. For every $0 \leq i \leq \tower(h) - 1$, $\mathcal{F}_i := \{\mathcal{T}_j \;|\; i \leq j \leq \tower(h) - 1\}$. Let $\mathcal{F} = \bigcup_{i=0}^{\tower(h)-1} \mathcal{F}_i$. Since $|\mathcal{F}| = \tower(h)$ and $|\theory_{f(c \cdot h^4)}(\mathfrak{T})| \leq \tower(h) - 1$, by pigeonhole principle, there are two numbers $a, b$ with $0 \leq a < b \leq \tower(h) - 1$ such that $\theory_{f(c \cdot h^4)}(\mathcal{F}_a) = \theory_{f(c \cdot h^4)}(\mathcal{F}_b)$. 
	
	Note that $\varphi_h$ holds for all $\mathcal{T}(n)$ with $n \in \mathbb{N}$. Hence, $\mathcal{F}_i \models \varphi_h$ for all $i \in \{0, ..., \tower(h) -1\}$. In addition, $\mathcal{F}_a \sqcup \mathcal{T}(a-1) = \mathcal{F}_{a-1}$. Therefore, $\mathcal{F}_a \sqcup \mathcal{T}(a-1) \models \varphi_h$. Moreover, $\mathcal{F}_b \sqcup \mathcal{T}(a-1) \not\models \varphi_h$ because there exists an $x \in \mathcal{T}(a-1)$ that can falsify $\encoding \rightarrow (\maxx \lor \exists y \; \succc )$.
	
	
	 From every set $\mathcal{F}_i$ we construct a new tree $\mathcal{U}_i$  by connecting a new root to all the roots of trees in $F_i$. Let $\mathcal{U} = \bigcup_{i=0}^{\tower(h)-1} \mathcal{U}_i$. The roots of both $\mathcal{U}_a$ and $\mathcal{U}_b$ have a child isomorphic to $\mathcal{T}(\tower(h)-1)$. By Lemma~\ref{tree-height-power} and Lemma~\ref{encoding}, both $\mathcal{U}_a$ and $\mathcal{U}_b$ encode a number bigger than $\tower(h)$. In other words, the roots of $\mathcal{U}_a$ and $\mathcal{U}_b$ do not satisfy $\encoding$. The rest of the nodes in $\mathcal{U}_a$ and $\mathcal{U}_b$ are $\mathcal{F}_a$ and $\mathcal{F}_b$, respectively. Therefore, $\mathcal{U}_a \sqcup \mathcal{T}(a-1) \models \varphi_h$ and $\mathcal{U}_b \sqcup \mathcal{T}(a-1) \not\models \varphi_h$.
	
\bibliographystyle{amsplain}
\bibliography{refs}

\end{document}



